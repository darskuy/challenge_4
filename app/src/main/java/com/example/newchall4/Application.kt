package com.example.newchall4

import android.app.Application
import androidx.room.Room
import com.example.newchall4.database.CartDatabase

class Application : Application() {
    companion object {
        lateinit var database: CartDatabase
    }

    override fun onCreate() {
        super.onCreate()

        // Initialize Room Database
        database = Room.databaseBuilder(
            applicationContext,
            CartDatabase::class.java, "database"
        ).build()
    }
}