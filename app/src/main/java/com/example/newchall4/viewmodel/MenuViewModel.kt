package com.example.newchall4.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.newchall4.Resource
import com.example.newchall4.api.ApiClient
import com.example.newchall4.menu.CategoryDataItem
import com.example.newchall4.menu.MenuListData
import com.example.newchall4.repository.MenuRepo
import com.example.newchall4.viewmodel.CartViewModelFactory


class MenuViewModel(private val menuRepository: MenuRepo) : ViewModel() {

    fun getMenuItems(): LiveData<Resource<List<MenuListData>>> {
        return menuRepository.getMenuItems()
    }

    fun getCategoryMenu(): LiveData<Resource<List<CategoryDataItem>>> {
        return menuRepository.getCategoryMenu()
    }

    class CartViewModelFactory(private val client: ApiClient) : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return when {
                modelClass.isAssignableFrom(MenuViewModel::class.java) -> {
                    val menuRepository = MenuRepo(client)
                    MenuViewModel(menuRepository) as T
                }
                else -> {
                    throw IllegalArgumentException("ViewModel tidak terdefinisi!")
                }
            }
        }
    }
}
